package com.honza.openshift.openshiftdemo.camelrest.dao;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Person {
    private final Long id;
    private final String name;
    private final Integer age;
}
