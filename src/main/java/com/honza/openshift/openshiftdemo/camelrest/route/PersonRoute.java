package com.honza.openshift.openshiftdemo.camelrest.route;

import com.honza.openshift.openshiftdemo.camelrest.dao.Person;
import com.honza.openshift.openshiftdemo.camelrest.service.PersonService;
import lombok.AllArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@AllArgsConstructor
public class PersonRoute extends RouteBuilder {

    private PersonService personService;

    @Override
    public void configure() throws Exception {
        restConfiguration().component("servlet")
                           .bindingMode(RestBindingMode.json);

        rest()
                .get()
                    .produces(MediaType.APPLICATION_JSON_VALUE)
                    .route()
                    .choice()
                        .when(exchange -> !StringUtils.isEmpty(personService.getMe()))
                            .setBody(constant("Hello world, welcome to app version 1.1. Welcome " + personService.getMe() + "!!!"))
                        .otherwise()
                            .setBody(constant("Hello world, welcome to app version 1.1"))
                        .end()
                .endRest()
        ;

        rest("/person")
                .get()
                    .produces(MediaType.APPLICATION_JSON_VALUE)
                    .route().setBody(() -> personService.getPersonList())
                .endRest()
                .get("/me")
                    .produces(MediaType.APPLICATION_JSON_VALUE)
                    .route().setBody(() -> personService.getMe())
                .endRest()
                .post()
                    .consumes(MediaType.APPLICATION_JSON_VALUE)
                    .type(Person.class)
                    .produces(MediaType.APPLICATION_JSON_VALUE)
                    .outType(Person.class)
                    .route()
                        .process(exchange -> personService.addPerson(exchange.getIn().getBody(Person.class)))
                .endRest()
        ;
    }
}
