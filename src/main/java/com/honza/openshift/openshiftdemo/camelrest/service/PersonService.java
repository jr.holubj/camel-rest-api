package com.honza.openshift.openshiftdemo.camelrest.service;

import com.honza.openshift.openshiftdemo.camelrest.dao.Person;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    @Value("${my.name}")
    private String myName;

    private final List<Person> personList = new ArrayList<>();

    public void addPerson(@NonNull final Person person) {
        personList.add(person);
    }

    public List<Person> getPersonList() {
        return new ArrayList<>(personList);
    }

    public String getMe() {
        return myName;
    }
}


